package jfy.slidez.game;

import java.util.ArrayList;

public class Sentence {
	private ArrayList<String> wordList;
	private String soundFile;
	
	public Sentence (ArrayList<String> wordList, String soundFile){
		this.wordList = wordList;
		this.soundFile = soundFile;
	}	
	
	public ArrayList<String> getWordList() {
		return wordList;	
	} 
	
	public String getFileName() {
		return soundFile;	
	}
}

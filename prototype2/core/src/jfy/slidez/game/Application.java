package jfy.slidez.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import jfy.slidez.game.screens.LoadingScreen;
import jfy.slidez.game.screens.MainMenuScreen;
import jfy.slidez.game.screens.PlayScreen;
import jfy.slidez.game.screens.PlayScreen2;
import jfy.slidez.game.screens.SplashScreen;

public class Application extends Game {

    public static final String TITLE = "Prototype";
    public static final float VERSION = .9f;
    public static final int V_WIDTH = 480;
    public static final int V_HEIGHT = 420;

    public OrthographicCamera camera;    
    public SpriteBatch batch;

    public BitmapFont font24;
    public AssetManager assets;
    
    public LoadingScreen loadingScreen;
    public SplashScreen splashScreen;
    public MainMenuScreen mainMenuScreen;
    public PlayScreen playScreen;
    public PlayScreen2 playScreen2;
    
    
    @Override
    public void create() {
    	assets = new AssetManager();
    	camera = new OrthographicCamera();
    	camera.setToOrtho(false, V_WIDTH, V_HEIGHT);
    	batch= new SpriteBatch();
    	
    	initFonts();
    	loadAssets();
    	
        loadingScreen = new LoadingScreen(this);
        splashScreen = new SplashScreen(this);
        mainMenuScreen = new MainMenuScreen(this);
        playScreen = new PlayScreen(this);
        playScreen2 = new PlayScreen2(this);
    	this.setScreen(loadingScreen);
    }
    
    private void initFonts() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Arcon.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();

        params.size = 24;
        //jfy: params.color = Color.BLACK;
        font24 = generator.generateFont(params);
    }

    private void loadAssets() {    	
    	//app.assets.load("ui/uiskin.atlas", TextureAtlas.class);
	}
    
    
    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
    	batch.dispose();
    	assets.dispose();
    	font24.dispose();
    	loadingScreen.dispose();
        splashScreen.dispose();
        mainMenuScreen.dispose();
        playScreen.dispose();
        playScreen2.dispose();
    }
    
}

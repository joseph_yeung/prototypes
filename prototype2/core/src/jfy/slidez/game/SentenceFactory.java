package jfy.slidez.game;

import java.util.ArrayList;


public abstract class SentenceFactory {	
	
	private SentenceFactory(){}
	//private static final ArrayList<String> wordList = new ArrayList<String>();

	
	public static Sentence getSentence(int level){	
		ArrayList<String> wordList = new ArrayList<String>();
		
		switch(level){
		case 1:
			wordList.add("> Tuna ndizi chache.");
			wordList.add("Hana/Tuna");
			wordList.add("ndizi/nyama");
			wordList.add("machache/chache");
			
			return new Sentence (wordList, "182A3.ogg");	
			
		case 2:
			wordList.add("> Tuna matunda machache.");
			wordList.add("Ana/Tuna");
			wordList.add("matunda/maziwa");
			wordList.add("machache/chache");
			
			return new Sentence (wordList, "182A5.ogg");
			
		case 3:	
			wordList.add("> Tuna nyama chache.");
			wordList.add("Tuna/Hana");
			wordList.add("ndizi/nyama");
			wordList.add("chache/machache");
			
			return new Sentence (wordList, "182A4.ogg");
		/*	
		case 4:	
			wordList.add("> Tuna viazi vichache.");
			wordList.add("Tana/Tuna");
			wordList.add("ndizi/viazi");
			wordList.add("machache/vichache");
			
			return new Sentence (wordList, "182A1.ogg");
				
		case 5:	
			wordList.add("> Tuna ndizi chache.");
			wordList.add("Tuna/Ana");
			wordList.add("ndizi/viazi");
			wordList.add("machache/chache");
			
			return new Sentence (wordList, "182A3.ogg");
			
			
		case 6:	
			wordList.add("> Tunahitaji maziwa mengi sasa.");
			wordList.add("Hatunahitaji/Tunahitaji");
			wordList.add("kahawa/maziwa");
			wordList.add("mengi/nyingi");
			wordList.add("sasa/jana");
			
			return new Sentence (wordList, "tunahitaji01.ogg");
		
		case 7:
			wordList.add("> Tunahitaji maziwa mengi sasa.");
			wordList.add("Hatunahitaji/Tunahitaji");
			wordList.add("kahawa/maziwa");
			wordList.add("mengi/nyingi");
			wordList.add("sasa/jana");
			
			return new Sentence (wordList, "tunahitaji02.ogg");	
		
		case 8:
			wordList.add("> Nyumba za kijiji hiki si za mawe!");
			wordList.add("nyumba/humba");
			wordList.add("ya/za");
			wordList.add("mji/kijiji");
			wordList.add("yake/hiki");
			wordList.add("ni sa udongo/si sa mawe");

			return new Sentence (wordList, "nyumba.ogg");	
		*/
		default:
			return new Sentence (wordList, "end");	
		}
		
	}
}

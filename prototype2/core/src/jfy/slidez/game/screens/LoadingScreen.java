package jfy.slidez.game.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import jfy.slidez.game.Application;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class LoadingScreen implements Screen {

	private final Application app;

	private ShapeRenderer shapeRenderer;
    private float progress;

    //jfy from Splash Screen
    private Texture splashTex;
    private Image splashImage;
	private Stage stage;

	//jfy
	Boolean finishedLoading;
	
	
	public LoadingScreen(final Application app){
		this.app = app;
		this.shapeRenderer = new ShapeRenderer();
		this.stage = new Stage(new FitViewport(Application.V_WIDTH, Application.V_HEIGHT, app.camera));		
		
		splashTex = new Texture(Gdx.files.internal("splash.png"));

	}

	private void queueAssets() {
		app.assets.load("splash.png", Texture.class);	
        app.assets.load("ui/uiskin.atlas", TextureAtlas.class);
        
        app.assets.load("sounds/nyumba.ogg", Sound.class);
        app.assets.load("sounds/tunahitaji01.ogg", Sound.class);
        app.assets.load("sounds/tunahitaji02.ogg", Sound.class);
        app.assets.load("sounds/182A1.ogg", Sound.class);
        app.assets.load("sounds/182A2.ogg", Sound.class);
        app.assets.load("sounds/182A3.ogg", Sound.class);
        app.assets.load("sounds/182A4.ogg", Sound.class);
        app.assets.load("sounds/182A5.ogg", Sound.class);
	
        app.assets.load("sounds/332D1.ogg", Sound.class);
        app.assets.load("sounds/332D2.ogg", Sound.class);
	}

	@Override
	public void show() {
		System.out.println("LOADING SCREEN");
		
		Runnable transitionRunnable = new Runnable() {
            @Override
            public void run() {
            	
            	if (finishedLoading == true)
            		app.setScreen(app.mainMenuScreen);
            }
        };
        
		splashImage = new Image(splashTex);		
		splashImage.setPosition(stage.getWidth()/2-32, stage.getHeight()/2+100);
		splashImage.setOrigin(splashImage.getWidth() / 2, splashImage.getHeight() / 2);
		splashImage.addAction(sequence(alpha(0), scaleTo(.1f, .1f),
                parallel(fadeIn(2f, Interpolation.pow2),
                        scaleTo(2f, 2f, 2.5f, Interpolation.pow5),
                        moveTo(stage.getWidth() / 2 - 32, stage.getHeight() / 2 - 32, 2f, Interpolation.swing)),
                delay(1.5f), fadeOut(1.25f), run(transitionRunnable)));
		
		stage.addActor(splashImage);
		
		shapeRenderer.setProjectionMatrix(app.camera.combined);
		this.progress = 0f;
        queueAssets();
        
        
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(.85f, .25f, .25f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		update(delta);

		stage.draw();
        
		app.batch.begin();
		app.font24.draw(app.batch, "LOADING", 20, 20);
		app.batch.end();
		
		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(32, app.camera.viewportHeight / 2 + 75, app.camera.viewportWidth - 64, 16);

        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.rect(32, app.camera.viewportHeight / 2 + 75, progress * (app.camera.viewportWidth - 64), 16);
        shapeRenderer.end();		
	}

	private void update(float delta) {
		stage.act(delta);

		progress = MathUtils.lerp(progress, app.assets.getProgress(), .1f);
        if (app.assets.update() && progress >= app.assets.getProgress() - .001f) {
            
        	finishedLoading = true;
        	//app.setScreen(app.splashScreen);
        }
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		shapeRenderer.dispose();
		stage.dispose();		
	}

}

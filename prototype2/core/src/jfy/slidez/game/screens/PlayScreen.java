package jfy.slidez.game.screens;


import java.util.ArrayList;

import jfy.slidez.game.Application;
import jfy.slidez.game.Sentence;
import jfy.slidez.game.SentenceFactory;
import jfy.slidez.game.SentenceFactory2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
//import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import jfy.slidez.game.actors.SlideButton;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class PlayScreen implements Screen {

	private final Application app;

    public MainMenuScreen mainMenuScreen;

	private Stage stage;
	private Skin skin;
	
	// Game Grid
    private int boardSize = 4;
    private int holeX, holeY; 
    private SlideButton[][] buttonGrid; 
    
    //jfy
    int level;
    int question; 
    TextButton choiceButton[] = new TextButton[3];
    Array<TextButton> buttons;
    boolean next;
    String fileName;
    String sentence;
    String[] words; 
    Long soundId;
    Sound readOutLoud; 
    
    private Label bigLabel;
 
	//jfy
	public ArrayList<String> wordList; 
   
	
	public PlayScreen(final Application app){
		this.app = app;
		this.stage = new Stage(new FitViewport(Application.V_WIDTH, Application.V_HEIGHT, app.camera));	
	}
	

	@Override
	public void show() {
		System.out.println("PLAY SCREEN");
		Gdx.input.setInputProcessor(stage);
		stage.clear();
		
		this.skin = new Skin();
        this.skin.addRegions(app.assets.get("ui/uiskin.atlas", TextureAtlas.class));
        this.skin.add("default-font", app.font24);
        this.skin.load(Gdx.files.internal("ui/uiskin.json"));

        level = 1;
        question =0;
        
        initWidgets();
        initData(level);
        createAnswerButtons();
	}

	private void initWidgets(){
		//init AnswerButtons
	    buttons = new Array<TextButton>();    
	    TextButton button1 = new TextButton("",skin, "default");
        buttons.add(button1);
        TextButton button2 = new TextButton("",skin, "default");
        buttons.add(button2);
        TextButton button3 = new TextButton("",skin, "default");
        buttons.add(button3);
        //init label for showing sentence
        bigLabel = new Label("", skin, "default");        
	}

	private void initData(int level) {
		Sentence s = SentenceFactory.getSentence(level);
		wordList = s.getWordList();
		fileName = s.getFileName();
		if (fileName == "end"){
			level = 1;   //reset beginning level 
			initData(level);  //otherwise initData(level) loops itself infinitely 
			app.setScreen(app.mainMenuScreen); //put up the mainMenuScreen again
		}	
	}   

	private void showSentence(String sentence, float time) {
		bigLabel.setText(sentence);
		bigLabel.setPosition(25, 310);
        bigLabel.addAction(sequence(alpha(0f), delay(.5f), fadeIn(.5f), fadeOut(time)));
        //labelInfo.setAlignment(Align.center);
        stage.addActor(bigLabel);
	}
	private void createAnswerButtons() {
		next = false;
		String s = wordList.get(question);
		String[] choices = s.split("/");	
		int wordLength;    //the string's length
		int numOfChoices = choices.length;          
		if (numOfChoices == 1){ //if that's a sentence, not word choices
			sentence = choices[0]; 			
			words = sentence.split(" ");
           	sentence = "";
			next = true;
        }
        for (int i=0; i<numOfChoices; i++)            
        {
        	wordLength = choices[i].length();        	
        	buttons.get(i).setText(choices[i]);
        	buttons.get(i).setPosition( 180, app.camera.viewportHeight - 200 - i*80);
        	buttons.get(i).setSize(wordLength*10 + 50, 50);
        	buttons.get(i).addListener(new ClickListener()  {
                
        		@Override
	            public void clicked(InputEvent event, float x, float y) {        			
        			next = true;
        		}
           });
           stage.addActor(buttons.get(i));
        }
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(.85f, .25f, .25f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		update(delta);
		
		stage.draw();
		
		app.batch.begin();
		app.font24.draw(app.batch, "PLAY SCREEN", 20, 20);
		app.batch.end();
	}

	private void update(float delta) {
		stage.act(delta);
		float f = 1.8f;
		
		if (next == true){    //makeshift nested if-statements
			sentence = sentence + " " + words[question];
			showSentence(sentence, f);
			
			if (soundId !=null)   //stop previously playing sound, if any
				readOutLoud.stop(soundId);
			
			readOutLoud = app.assets.get("sounds/" + fileName, Sound.class);
			soundId = readOutLoud.play();
	        
			question++; 
			Gdx.app.log("question" , " "+ question);
			
			if (question == wordList.size()){
	       		question=0;  //reset question pointer
	       		level++; 
				initData(level);
	       	}			
			createAnswerButtons();
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	} 

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}

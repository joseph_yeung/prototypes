package jfy.slidez.game.screens;

import jfy.slidez.game.Application;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FitViewport;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class SplashScreen implements Screen {

	private final Application app;
	private Stage stage;

	private Image splashImage;
	
	public SplashScreen(final Application app){
		this.app = app;
		this.stage = new Stage(new FitViewport(Application.V_WIDTH, Application.V_HEIGHT, app.camera));		
	}
	
	@Override
	public void show() {
		System.out.println("SPLASH SCREEN");
		Gdx.input.setInputProcessor(stage);

		Runnable transitionRunnable = new Runnable() {
            @Override
            public void run() {
                app.setScreen(app.mainMenuScreen);
            }
        };

		Texture splashTex = app.assets.get ("splash.png", Texture.class);
		splashImage = new Image(splashTex);		
		splashImage.setPosition(stage.getWidth()/2-32, stage.getHeight()/2-32);
		splashImage.setOrigin(splashImage.getWidth() / 2, splashImage.getHeight() / 2);
		splashImage.addAction(sequence(alpha(0), scaleTo(.1f, .1f),
                parallel(fadeIn(2f, Interpolation.pow2),
                        scaleTo(2f, 2f, 2.5f, Interpolation.pow5),
                        moveTo(stage.getWidth() / 2 - 32, stage.getHeight() / 2 - 32, 2f, Interpolation.swing)),
                delay(1.5f), fadeOut(1.25f), run(transitionRunnable)));
		
		stage.addActor(splashImage);
	}


	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(.85f, .25f, .25f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update(delta);

        stage.draw();
        
        app.batch.begin();
		app.font24.draw(app.batch, "SPLASH", 20, 20);
		app.batch.end();			
	}
	
	public void update(float delta){
		stage.act(delta);
	}

	@Override
	public void resize(int width, int height) {
        stage.getViewport().update(width, height, false);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();		
	}

}

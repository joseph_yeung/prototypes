package jfy.slidez.game.screens;


import java.util.ArrayList;

import jfy.slidez.game.Application;
import jfy.slidez.game.Sentence;
import jfy.slidez.game.SentenceFactory2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import jfy.slidez.game.actors.SlideButton;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class PlayScreen2 implements Screen {

	private final Application app;

    public MainMenuScreen mainMenuScreen;

	private Stage stage;
	private Skin skin;
	  
    //jfy
    int level;
    int wordNum; 
    boolean reading;
    
    String fileName;
    String sentence = "";
    String words = ""; 
    Long soundId;
    Sound readOutLoud; 
    Label bigLabel;
    
    TextButton buttonPlay;
	TextButton buttonTryMe;
	TextButton buttonPlayAgain;
	
 
	//jfy
	ArrayList<String> wordList; 
	float timer; 
	Table table;
	Table table2;
	
	public PlayScreen2(final Application app){
		this.app = app;
		this.stage = new Stage(new FitViewport(Application.V_WIDTH, Application.V_HEIGHT, app.camera));		
		
		//create widgets here once 
		table = new Table();
	}

	@Override
	public void show() {
		System.out.println("PLAY SCREEN");
		Gdx.input.setInputProcessor(stage);
		stage.clear();
		
		
		this.skin = new Skin();
        this.skin.addRegions(app.assets.get("ui/uiskin.atlas", TextureAtlas.class));
        this.skin.add("default-font", app.font24);
        this.skin.load(Gdx.files.internal("ui/uiskin.json"));

        level = 1;
        wordNum = 1;
        
        initData(level);
         //init label for showing sentence
        bigLabel = new Label("", skin, "default");
        bigLabel.setWrap(true);
		
        //create widgets here once 
      	table = new Table();
      	table2 = new Table();
        
      	initTable();
	    setPlayButton();		
	}
	
    private void setPlayButton(){
    	buttonPlay = new TextButton("Play",skin, "default");
        buttonPlay.padLeft(50);
        buttonPlay.padRight(50);
        buttonPlay.setPosition( 180, app.camera.viewportHeight - 160);
        buttonPlay.setSize(120, 35);
        //jfy -- add action 
        buttonPlay.addAction(sequence(alpha(0), parallel(fadeIn(.5f), moveBy(0, -20, .5f, Interpolation.pow5Out))));
        buttonPlay.addListener(new ClickListener()  {
    		@Override
            public void clicked(InputEvent event, float x, float y) {        			
    			reading = true;
    			read();
    			setPlayAgain();
    			setTryMe();
    		}
       });
       stage.addActor(buttonPlay);
    };

	private void setPlayAgain(){
		buttonPlay.remove();   //to be replaced by Again 
		
		buttonPlayAgain = new TextButton("Again",skin, "default");
        buttonPlayAgain.padLeft(50);
        buttonPlayAgain.padRight(50);
        buttonPlayAgain.setPosition( 180, app.camera.viewportHeight - 160);
        buttonPlayAgain.setSize(120, 35);
        //jfy -- add action 
        buttonPlayAgain.addAction(sequence(alpha(0), parallel(fadeIn(.5f), moveBy(0, -20, .5f, Interpolation.pow5Out))));
        buttonPlayAgain.addListener(new ClickListener()  {
        	@Override
            public void clicked(InputEvent event, float x, float y) {        			
    			reading = true;
    			read();
        	}
       });
       stage.addActor(buttonPlayAgain);
	}
	
	private void setTryMe(){
		buttonTryMe = new TextButton("Try me",skin, "default");
		buttonTryMe.padLeft(50);
        buttonTryMe.padRight(50);
        buttonTryMe.setPosition( 180, app.camera.viewportHeight - 200);
        buttonTryMe.setSize(120, 35);
        //jfy -- add action 
        buttonTryMe.addAction(sequence(alpha(0), parallel(fadeIn(.5f), moveBy(0, -20, .5f, Interpolation.pow5Out))));
        buttonTryMe.addListener(new ClickListener()  {
    		@Override
            public void clicked(InputEvent event, float x, float y) {        			
    			//reset to run the sentence again
    			timer = 0;
    			buttonTryMe.remove();
    			buttonPlayAgain.remove();
    			toAssessRead();
    		    setPlayButton();		
    			//prepare new sentence
    			timer = 0;
	       		level++;
				initData(level);
    		}
       });
       stage.addActor(buttonTryMe);
	}
	
	private void toAssessRead(){
		String message = "";
		message += "Player reads. Now, we are to detect player's voice";
		message += ", then count the DURATION. "; 
		message += "If it matches the sample's time, then pass, otherwise repeat.";
		
		Label explain = new Label("", skin);
	    explain.setWrap(true);
	    explain.setText(message); // the player's and start counting time once it goes off
	    explain.addAction(sequence(alpha(0),color(new Color(0f, 0f, 0f, .7f), 3f)));
	    
	    table2 = new Table();
	    table2.setWidth(stage.getWidth()-600);
	    table2.align(Align.left | Align.top);
	    table2.setPosition(0,230);
	    table2.padTop(30);
        table2.padLeft(60);
        table2.add(explain).width(350f);// <--- here you define the width! not ^
	    
	    stage.addActor(table2);
	}
	
	private void initData(int level) {
		Sentence s = SentenceFactory2.getSentence(level);
		wordList = s.getWordList();
		fileName = s.getFileName();
		if (fileName == "end"){
			table.remove();
			table2.remove();
			level = 1;   //reset beginning level 
			initData(level);  //otherwise initData(level) loops itself infinitely 
			app.setScreen(app.mainMenuScreen); //put up the mainMenuScreen again
		}
	} 
	
	private void initTable() {
	    table.setWidth(stage.getWidth()-600);
	    table.align(Align.left | Align.top);
	    table.setPosition(0,Gdx.graphics.getHeight());

	    table.padTop(30);
        table.padLeft(60);
        table.add(bigLabel).width(350f);// <--- here you define the width! not ^

        stage.addActor(table);
	} 

	private int countLetters(String s){
		int counter = 0;
		for( int i=0; i<s.length(); i++ ) {
		    if(Character.isLetter(s.charAt(i))) {
		        counter++;
		    } 
		}
		return counter;	
	}

	private void read(){		
		readOutLoud = app.assets.get("sounds/" + fileName, Sound.class);
		soundId = readOutLoud.play();
	}
	
	private void showWhileRead(String sentence) {
		bigLabel.setText(sentence);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(.85f, .25f, .25f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		update(delta);
		
		stage.draw();
		
		app.batch.begin();
		app.font24.draw(app.batch, "PLAY SCREEN", 20, 20);
		app.batch.end();
	}

	private void update(float delta) {
		stage.act(delta);
		float wordTime; 
		 
		words = wordList.get(wordNum); //wordNum =1, get(0) is the sentence
		
		if (reading == true){    //makeshift nested if-statements
			timer = timer + delta; 
			
			wordTime = 0.04f * countLetters(words);
			
			if (timer > wordTime){
				timer = timer - wordTime; //reset the timer to reminder
				sentence = sentence + words + " ";
				showWhileRead(sentence);  //f is not in effect right now
				wordNum++; 
				Gdx.app.log("word number" , " "+ wordNum);
			}
			
			if (wordNum == wordList.size()){ //meaning, if all words in the sentence are read
	       		wordNum = 1;  //reset word pointer
	       		reading = false;
	       		sentence = "";
	       		if(buttonPlayAgain!=null) 
	       			setPlayAgain();
	       	}			
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	} 

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}


//        buttonPlay.addAction(sequence(alpha(0), parallel(fadeIn(.5f), moveBy(0, -20, .5f, Interpolation.pow5Out))));

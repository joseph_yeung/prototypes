package jfy.slidez.game;

import java.util.ArrayList;


public abstract class SentenceFactory2 {	
	
	private SentenceFactory2(){}
	//private static final ArrayList<String> wordList = new ArrayList<String>();

	
	public static Sentence getSentence(int level){
		//TODO:just clear wordList (create above) rather than create a new one  
		ArrayList<String> wordList = new ArrayList<String>();
		
		switch(level){
				
		case 1:
			wordList.add("> Watoto wengine hawana chakula cha kutosha mwaka huu.");
			splitSentence(wordList);		
			return new Sentence (wordList, "332D1.ogg");	
		
		case 2:
			wordList.add("> Ng'ombe wengine hawana chakula cha kutosha mwaka huu.");
			splitSentence(wordList);		
			return new Sentence (wordList, "332D2.ogg");	
		
		default:
			return new Sentence (wordList, "end");	
		}
		
	}
	
	private static void splitSentence(ArrayList<String> wordList){
		String sentence = wordList.get(0);
		String[] words = sentence.split(" ");	
		for( int i=0; i<words.length; i++ ) {
			wordList.add(words[i]);
		}    
    }
}
